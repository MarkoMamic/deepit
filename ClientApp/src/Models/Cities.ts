export class Cities {
    cityID?: number;
    cityName: string;
    population: number;
    countryName: string;
  }
