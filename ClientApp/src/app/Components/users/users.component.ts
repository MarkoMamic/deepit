import { Component } from '@angular/core';
import { first } from 'rxjs/operators';

import { User } from '../../../Models/User';
import { UserService } from '../../Services/user.service';
import { AuthenticationService } from '../../Services/authentification.service';

@Component({
    templateUrl: 'users.component.html',
    styleUrls: ['users.component.scss'],
    selector: 'app-users'
})
export class UsersComponent {
    loading = false;
    users: User[];

    constructor(private userService: UserService) { }

    ngOnInit() {
        this.loading = true;
        this.userService.getAll().pipe(first()).subscribe(users => {
            this.loading = false;
            this.users = users;
        });
    }
}