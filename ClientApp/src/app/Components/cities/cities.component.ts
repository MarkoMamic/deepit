import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {GeoInformationService} from '../../Services/geo-information.service';
import {Cities} from '../../../Models/Cities';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.scss']
})
export class CitiesComponent implements OnInit {

  cities$: Observable<Cities[]>;

  constructor(private citiesService: GeoInformationService) {
  }

  ngOnInit() {
    this.loadCities();
  }

  loadCities() {
    this.cities$ = this.citiesService.getCities();
  }

  deleteCity(cityID) {
    const ans = confirm('Do you want to delete the city with id: ' + cityID);
    if (ans) {
      this.citiesService.deleteCity(cityID).subscribe((data) => {
        this.loadCities();
      });
    }
  }
}


