import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { GeoInformationService } from '../../Services/geo-information.service';
import { Cities } from 'src/Models/Cities';


@Component({
  selector: 'app-edit-city',
  templateUrl: './edit-city.component.html',
  styleUrls: ['./edit-city.component.scss']
})
export class EditCityComponent implements OnInit {
  form: FormGroup;
  actionType: string;
  formCityName: string;
  formPopulation: string;
  formCountryName: string;
  cityId: number;
  errorMessage: any;
  existingCity: Cities;

  constructor(
    private geoInformationService: GeoInformationService,
    private formBuilder: FormBuilder,
    private avRoute: ActivatedRoute,
    private router: Router) {
    const idParam = 'id';
    this.actionType = 'Add';
    this.formCityName = 'cityName';
    this.formPopulation = 'population';
    this.formCountryName = 'countryName';
    if (this.avRoute.snapshot.params[idParam]) {
      this.cityId = this.avRoute.snapshot.params[idParam];
    }
    this.form = this.formBuilder.group(
      {
         cityID: 0,
         cityName: ['', [Validators.required]],
         population: Number(['', [Validators.required]]),
         countryName: ['', [Validators.required]]
      }
    );
  }
  ngOnInit() {
    if (this.cityId > 0) {
      this.actionType = 'Edit';
      this.geoInformationService.getCity(this.cityId)
        .subscribe(data => (
          this.existingCity = data,
          this.form.controls[this.formCityName].setValue(data.cityName),
          this.form.controls[this.formPopulation].setValue(data.population),
          this.form.controls[this.formCountryName].setValue(data.countryName)
        ));
    }
  }
  save() {
    if (!this.form.valid) {
      return;
    }

    if (this.actionType === 'Add') {
      let cities: Cities = {
        cityName: this.form.get(this.formCityName).value,
        population:  Number(this.form.get(this.formPopulation).value),
        countryName: this.form.get(this.formCountryName).value
      };

      this.geoInformationService.saveCity(cities)
        .subscribe((data) => {
          this.router.navigate(['/city/', data]);
        });
    }


    if (this.actionType === 'Edit') {
      let cities: Cities = {
        cityID: this.existingCity.cityID,
        cityName: this.form.get(this.formCityName).value,
        population: Number(this.form.get(this.formPopulation).value),
        countryName: this.form.get(this.formCountryName).value,

      };
      this.geoInformationService.updateCity(cities, cities.cityID)
        .subscribe((data) => {
          this.router.navigate([this.router.url]);
        });
    }
  }

  cancel() {
    this.router.navigate(['/']);
  }

  get cityName() { return this.form.get(this.formCityName); }
  get population() { return this.form.get(this.formPopulation); }
  get countryName() { return this.form.get(this.formCountryName); }
}
