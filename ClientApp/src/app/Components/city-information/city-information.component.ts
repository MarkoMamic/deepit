import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Cities } from 'src/Models/Cities';
import { Observable } from 'rxjs';
import {GeoInformationService} from '../../Services/geo-information.service';

@Component({
  selector: 'app-city-information',
  templateUrl: './city-information.component.html',
  styleUrls: ['./city-information.component.scss']
})
export class CityInformationComponent implements OnInit {
  city$: Observable<Cities>;
  cityId: number;
  constructor(private geoInformationService: GeoInformationService, private avRoute: ActivatedRoute) {
    const idParam = 'id';
    if (this.avRoute.snapshot.params[idParam]) {
      this.cityId = this.avRoute.snapshot.params[idParam];
    }
  }

  ngOnInit() {
    this.loadCity();
  }

  loadCity() {
    this.city$ = this.geoInformationService.getCity(this.cityId);
  }
}
