import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditCityComponent } from './Components/edit-city/edit-city.component';
import { CitiesComponent } from './Components/cities/cities.component';
import { CityInformationComponent } from './Components/city-information/city-information.component';
import { UsersComponent } from './Components/users/users.component';
import { LoginComponent } from './Components/login/login.component';
import { AuthGuard } from './_helpers/auth.guard';
import { from } from 'rxjs';

const routes: Routes = [
  { path: '', component: CitiesComponent, pathMatch: 'full', canActivate: [AuthGuard] },
  { path: 'city/edit/:id', component: EditCityComponent , canActivate: [AuthGuard] },
  { path: 'city/:id', component: CityInformationComponent, canActivate: [AuthGuard] } ,
  { path: 'add', component: EditCityComponent, canActivate: [AuthGuard]  },
  // { path: '', component: UsersComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: '**', redirectTo: '/' }
];







@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
