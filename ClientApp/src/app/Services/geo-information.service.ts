import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Cities } from '../../Models/Cities';

@Injectable({
  providedIn: 'root'
})
export class GeoInformationService {

  myAppUrl: string;
  myApiUrl: string;
  httpOptions = {
    headers: new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Content-Type': 'application/json; charset=utf-8'
    })
  };
  constructor(private http: HttpClient) {
      this.myAppUrl = environment.appUrl;
      this.myApiUrl = 'api/cities/';
  }

  getCities(): Observable<Cities[]> {
    return this.http.get<Cities[]>(this.myAppUrl + this.myApiUrl)
    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );
  }

  getCity(cityID: number): Observable<Cities> {
      return this.http.get<Cities>(this.myAppUrl + this.myApiUrl + cityID)
      .pipe(
        retry(1),
        catchError(this.errorHandler)
      );
  }

  updateCity(cities: Cities, id: number): Observable<Cities> {
      return this.http.put<Cities>(this.myAppUrl + this.myApiUrl + id, JSON.stringify(cities), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.errorHandler)
      );
  }

  saveCity(cities: Cities): Observable<Cities> {
    return this.http.post<Cities>(this.myAppUrl + this.myApiUrl, JSON.stringify(cities), this.httpOptions)
    .pipe(
      retry(1),
      catchError(this.errorHandler)
    );
}

  deleteCity(cityID: number): Observable<Cities> {
      return this.http.delete<Cities>(this.myAppUrl + this.myApiUrl + cityID)
      .pipe(
        retry(1),
        catchError(this.errorHandler)
      );
  }

  errorHandler(error: { error: { message: string; }; status: any; message: any; }) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(errorMessage);
    return throwError(errorMessage);
  }
}
