import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { User } from '../../Models/User';



@Injectable({ providedIn: 'root' })
export class UserService {
    httpOptions = {
        headers: new HttpHeaders({
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json; charset=utf-8'
        })
      };
    constructor(private http: HttpClient) { }

    getAll() {
        return this.http.get<User[]>(`${environment.appUrl}users`, this.httpOptions);
    }
}
