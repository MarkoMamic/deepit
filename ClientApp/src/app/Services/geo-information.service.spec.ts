import { TestBed } from '@angular/core/testing';

import { GeoInformationService } from './geo-information.service';

describe('GeoInformationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GeoInformationService = TestBed.get(GeoInformationService);
    expect(service).toBeTruthy();
  });
});
