﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeoInformation.Business.Helpers
{
   public class AppSettings
    {
        public string Secret { get; set; }
        public ConnectionString ConnectionString { get; set; }

    }
}
