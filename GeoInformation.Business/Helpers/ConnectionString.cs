﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeoInformation.Business.Helpers
{
   public class ConnectionString
    {
        public string DatabaseConnectionString { get; set; }
    }
}
