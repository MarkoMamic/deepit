﻿using GeoInformation.Business;
using GeoInformation.Business.Models;
using System.Linq;

namespace GeoInformation.DataAccess
{
    public static class DbInitializer
    {
        public static void Initialize(MainEntities context)
        {
            context.Database.EnsureCreated();

            if (context.Cities.Any())
            {
                return;
            }

            var cities = new City[]
            {
                new City{CityName="Zagreb",Population =803900, CountryName = "Croatia" },
                new City{CityName="Rijeka",Population=128624, CountryName = "Croatia" },
                new City{CityName="Split",Population=178192, CountryName = "Croatia" },
                new City{CityName="Sisak",Population=47768, CountryName = "Croatia" },
                new City{CityName="Nürnberg",Population=515200, CountryName = "Germany" },
                new City{CityName="Berlin",Population=3748000, CountryName = "Germany" },
                new City{CityName="Hamburg",Population=1822000, CountryName = "Germany" },
                new City{CityName="Paris",Population=2400000, CountryName = "France" }
            };
            foreach (City c in cities)
            {
                context.Cities.Add(c);
            }
            context.SaveChanges();


            var user = new User { Id = 1, FirstName = "Test", LastName = "User", Username = "test", Password = "test" };
            context.Users.Add(user);




            context.SaveChanges();

        }
    }
}
