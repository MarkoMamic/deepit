﻿using GeoInformation.Business;
using GeoInformation.Business.Models;
using Microsoft.EntityFrameworkCore;

namespace GeoInformation.DataAccess
{
    public partial class MainEntities : DbContext
    {


        public MainEntities(DbContextOptions<MainEntities> options)
            : base(options)
        {
            
        }
        public DbSet<User> Users { get; set; }
        public DbSet<City> Cities { get; set; }

    
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<City>().ToTable("Cities").Property(p => p.CityID).ValueGeneratedOnAdd();     
            modelBuilder.Entity<User>().ToTable("Users");        
            
        }
    }
}

